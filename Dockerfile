FROM debian:jessie-slim
MAINTAINER Alexander Ryapolov <rayu@goldmc.ru>

RUN apt-get update
RUN apt-get dist-upgrade -y
RUN apt-get install -y wget build-essential libgl1-mesa-dev libfontconfig1 libdbus-1-3
COPY install.qs /tmp
RUN wget http://download.qt.io/official_releases/online_installers/qt-unified-linux-x64-online.run
RUN chmod a+x ./qt-unified-linux-x64-online.run
RUN ./qt-unified-linux-x64-online.run --platform minimal --script /tmp/install.qs
RUN rm ./qt-unified-linux-x64-online.run
RUN rm /tmp/install.qs
CMD ["/bin/bash"]
